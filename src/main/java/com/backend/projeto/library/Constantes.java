package com.backend.projeto.library;

public class Constantes {
    public final static String MATRICULADOR = "ROLE_MATRICULADOR";
    public final static String ADMIN = "ROLE_ADMIN";
    public final static String PROFESSOR = "ROLE_PROFESSOR";
    public final static String LIDER = "ROLE_LIDER";
    public final static String ALUNO = "ROLE_ALUNO";
    public final static String CADASTRADO = "ROLE_CADASTRADO";


    //Senha
    public final static String SENHA_PADRAO = "123456";



}