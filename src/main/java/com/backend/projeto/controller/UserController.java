package com.backend.projeto.controller;


import com.backend.projeto.controller.dto.UserCurrentDTO;
import com.backend.projeto.library.Constantes;
import com.backend.projeto.repository.UserRepository;
import com.backend.projeto.service.UserRepositoryUserDetails;
import com.backend.projeto.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    public UserService usuarioService;

    @Autowired
    public UserRepository usuarioRepository;


    @Autowired
    private TokenStore tokenStore;


    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public void logout(HttpServletRequest request) {
        String authHeader = request.getHeader("Authorization");
        if (authHeader != null) {
            String tokenValue = authHeader.replace("Bearer", "").trim();
            OAuth2AccessToken accessToken = tokenStore.readAccessToken(tokenValue);
            tokenStore.removeAccessToken(accessToken);
        }
    }



    @GetMapping(value = "/current")
    @Secured({Constantes.ADMIN})
    public UserCurrentDTO currentUser(
    ) {
        UserCurrentDTO usuarioAtualDTO = new UserCurrentDTO();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return usuarioAtualDTO.getUsuarioDTO((UserRepositoryUserDetails) authentication.getPrincipal());
    }


}
