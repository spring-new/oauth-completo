package com.backend.projeto.controller.dto;

import com.backend.projeto.entity.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.util.List;

@Getter
@Setter
public class UserCurrentDTO {

    private Long id;

    private String name;

    private String email;

    private String cpf;

    public UserCurrentDTO getUsuarioDTO(User usuario) {
        BeanUtils.copyProperties(usuario, this);
        return this;
    }

}
