package com.backend.projeto.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import java.io.Serializable;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OauthAccessToken implements Serializable {

    @Id
    private String authenticationId;

    private String tokenId;

    @Lob
    private byte[] token;

    private String userName;

    private String clientId;

    @Lob
    private byte[] authentication;

    private String refreshToken;


}
