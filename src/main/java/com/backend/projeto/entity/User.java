package com.backend.projeto.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Setter
@Getter
//@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "increment")
    private Long id;

    @Column(unique = true)
    private String name;

    private String email;

    private String password;

    @Column(unique = true)
    private String cpf;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private List<Role> roles;


    public User(User user) {
        super();
        this.id = user.getId();
        this.name = user.getName();
        this.email = user.getEmail();
        this.cpf = user.getCpf();
        this.roles = user.getRoles();
        this.password = user.getPassword();
    }

    public User(String name, String email, String password, String cpf, List<Role> roles) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.cpf = cpf;
        this.roles = roles;
    }
}
