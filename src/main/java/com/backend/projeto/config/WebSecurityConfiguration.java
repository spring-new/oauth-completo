package com.backend.projeto.config;


import com.backend.projeto.service.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private MyUserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception{
        return super.authenticationManagerBean();
    }

    @Override
    public void configure(WebSecurity webSecurity) throws Exception{
        webSecurity.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
        webSecurity.ignoring().antMatchers(HttpMethod.GET, "/swagger-ui**");
        webSecurity.ignoring().antMatchers(HttpMethod.GET, "/webjars/**");
        webSecurity.ignoring().antMatchers(HttpMethod.GET, "/swagger**");
        webSecurity.ignoring().antMatchers(HttpMethod.GET, "/v2/**");
        webSecurity.ignoring().antMatchers(HttpMethod.GET, "/swagger-resources/**");
        webSecurity.ignoring().antMatchers(HttpMethod.GET, "/csrf");
        webSecurity.ignoring().antMatchers(HttpMethod.GET, "/h2-console/**");
        webSecurity.ignoring().antMatchers(HttpMethod.GET, "/h2-console");
        webSecurity.ignoring().antMatchers(HttpMethod.POST, "/h2-console");
        webSecurity.ignoring().antMatchers(HttpMethod.POST, "/h2-console/login.do");
        webSecurity.ignoring().antMatchers(HttpMethod.POST, "/h2-console/query.do");
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }


}
