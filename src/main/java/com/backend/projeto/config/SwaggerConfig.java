package com.backend.projeto.config;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {


    @Value("${host.full.dns.auth.link}")
    private String authLink;

    @Bean
    public Docket api() {

        List<ResponseMessage> list = new java.util.ArrayList<>();
        list.add(new ResponseMessageBuilder().code(500).message("500 message")
                .responseModel(new ModelRef("Result")).build());
        list.add(new ResponseMessageBuilder().code(401).message("Unauthorized")
                .responseModel(new ModelRef("Result")).build());
        list.add(new ResponseMessageBuilder().code(406).message("Not Acceptable")
                .responseModel(new ModelRef("Result")).build());

        return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
                .paths(errors())
                .paths(oauthCheckToken())
                .paths(oauthToken())
                .paths(oauthConfirmToken())
                .paths(oauthError())
                .paths(oauthAuthorize())
                .build().securitySchemes(Collections.singletonList(securitySchema()))
                .securityContexts(Collections.singletonList(securityContext())).pathMapping("/")
                .useDefaultResponseMessages(false).apiInfo(apiInfo()).globalResponseMessage(RequestMethod.GET, list)
                .globalResponseMessage(RequestMethod.POST, list);

    }

    private Predicate<String> errors() {
        return Predicates.not(PathSelectors.regex("/error*"));
    }

    private Predicate<String> cadastrar() {
        return Predicates.not(PathSelectors.regex("/usuario/cadastro*"));
    }

    private Predicate<String> ativar() {
        return Predicates.not(PathSelectors.regex("/usuario/ativar*"));
    }

    private Predicate<String> reenviar() {
        return Predicates.not(PathSelectors.regex("/usuario/reenviar-codigo*"));
    }

    private Predicate<String> oauthAuthorize() {
        return Predicates.not(PathSelectors.regex("/oauth/authorize*"));
    }

    private Predicate<String> oauthCheckToken() {
        return Predicates.not(PathSelectors.regex("/oauth/check_token*"));
    }

    private Predicate<String> oauthToken() {
        return Predicates.not(PathSelectors.regex("/oauth/token*"));
    }

    private Predicate<String> oauthConfirmToken() {
        return Predicates.not(PathSelectors.regex("/oauth/confirm_access*"));
    }

    private Predicate<String> oauthError() {
        return Predicates.not(PathSelectors.regex("/oauth/error*"));
    }

    private OAuth securitySchema() {

        List<AuthorizationScope> authorizationScopeList = newArrayList();
        authorizationScopeList.add(new AuthorizationScope("read", "read all"));
        authorizationScopeList.add(new AuthorizationScope("trust", "trust all"));
        authorizationScopeList.add(new AuthorizationScope("write", "access all"));

        List<GrantType> grantTypes = newArrayList();
        GrantType creGrant = new ResourceOwnerPasswordCredentialsGrant(authLink + "/oauth/token");

        grantTypes.add(creGrant);

        return new OAuth("oauth2schema", authorizationScopeList, grantTypes);

    }

    private SecurityContext securityContext() {
        return
                SecurityContext.builder().securityReferences(defaultAuth())
                        .forPaths(PathSelectors.ant("/**"))
                        .forPaths(cadastrar())
                        .forPaths(reenviar())
                        .forPaths(ativar())
                        .build();
    }

    private List<SecurityReference> defaultAuth() {

        final AuthorizationScope[] authorizationScopes = new AuthorizationScope[3];
        authorizationScopes[0] = new AuthorizationScope("all", "read all");
        authorizationScopes[1] = new AuthorizationScope("trust", "trust all");
        authorizationScopes[2] = new AuthorizationScope("write", "write all");

        return Collections.singletonList(new SecurityReference("oauth2schema", authorizationScopes));
    }

    @Bean
    public SecurityConfiguration securityInfo() {
        return new SecurityConfiguration("mobile", "123456", "", "", "", ApiKeyVehicle.HEADER, "", " ");
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("API Orquestrador projeto COVID19").description("")
                .termsOfServiceUrl("https://projetoconivd....")
                .contact(new Contact("Ministério da Saúde", "https://natanielpaiva.github.io/", "nataniel.paiva@gmail.com"))
                .license("Ministério da Saúde").licenseUrl("https://consultorianjl.com").version("1.0.0").build();
    }

}